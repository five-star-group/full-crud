package org.example.Full_Crud;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.example.Base.BaseTest;
import org.example.COMMON.APIConstants;
import org.example.Resources.PayloadManager;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Integration extends BaseTest {
    String bookingid;
    String token;

    @Test
    public void CreateBooking() throws JsonProcessingException {
        token=getToken();
        requestSpecification.basePath(APIConstants.Create_Booking);
        response=requestSpecification.body(payloadManager.CreateBooking()).when().post();
        System.out.println(response.asString());
        jsonPath=JsonPath.from(response.asString());
        bookingid=jsonPath.getString("bookingid");

    }
    @Test(dependsOnMethods = "CreateBooking")
    public void UpdateBooking() throws JsonProcessingException {
        token=getToken();
        requestSpecification.basePath(APIConstants.Update_Booking+"/"+bookingid);
        response=requestSpecification.cookie("token",token).body(payloadManager.UpdateBooking()).when().put();
        System.out.println(response.asString());

    }

    @Test(dependsOnMethods = "UpdateBooking")
    public void PatchBooking() throws JsonProcessingException {
        token=getToken();
        requestSpecification.basePath(APIConstants.Update_Booking+"/"+bookingid);
        response=requestSpecification.cookie("token",token).body(payloadManager.PatchBooking()).when().patch();
        System.out.println(response.asString());

    }
    @Test(dependsOnMethods = "PatchBooking")
    public void DeleteBooking() throws JsonProcessingException {
        token=getToken();
        requestSpecification.basePath(APIConstants.Update_Booking+"/"+bookingid).cookie("token",token);
        response=requestSpecification.auth().basic("admin","password123").when().delete();
        System.out.println(response.asString());

    }
    @Test(dependsOnMethods = "DeleteBooking")
    public void GetBooking() throws JsonProcessingException {
        requestSpecification.basePath(APIConstants.Update_Booking+"/"+bookingid);
        response=requestSpecification.when().get();
        System.out.println(response.asString());

    }
}
